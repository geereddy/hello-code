TARGET=hello
CC=gcc
OBJS= hello.o

all: $(TARGET)

%.o: %.c
	$(CC) $(CFLAGS) -c $<

hello: $(OBJS)
	$(CC) $(CFLAGS) -o $@ $(OBJS)

